#include "mainwindow.h"
#include "dataOperation.h"
#include "aggHelper.h"
#include <QApplication>
#include "date.h"
#include <QCoreApplication>
#include <QDebug>

date* date::knowday;
list<package> dataOperation::allPackage;
list<express> dataOperation::allExpress;
shared_ptr<tableManager> tableManager::tablemanager;
shared_ptr<aggHelper> aggHelper::helper;
string table::default_path;

int main(int argc, char *argv[])
{
    table::default_path="D:\\personal_file\\download_files\\express_data";
    /*shared_ptr<col> courierNumber=shared_ptr<col>(new col(STR,"courierNumber"));
    shared_ptr<col> consignor=shared_ptr<col>(new col(STR,"consignor"));
    shared_ptr<col> consignee=shared_ptr<col>(new col(STR,"consignee"));
    shared_ptr<col> name=shared_ptr<col>(new col(STR,"name"));
    shared_ptr<col> weight=shared_ptr<col>(new col(FLOAT,"weight"));
    shared_ptr<col> volume=shared_ptr<col>(new col(FLOAT,"volume"));
    shared_ptr<col> value=shared_ptr<col>(new col(FLOAT,"value"));
    //shared_ptr<col> pickupCode=shared_ptr<col>(new col(STR,"pickupCode"));
    shared_ptr<col> isOutStock=shared_ptr<col>(new col(BOOL,"isPaid"));
    shared_ptr<table> allPackages=shared_ptr<table>(new table("allExpress",{courierNumber,consignor,consignee,name,weight,volume,value,isOutStock}));
    allPackages->saveFile();*/
    tableManager::tablemanager=tableManager::getTableManager();
    aggHelper::helper=aggHelper::getHelper();
    QApplication a(argc, argv);
    MainWindow w;
    dataOperation::init();
    //w.path="/storage/emulated/0/save"; //android保存
    w.path=QCoreApplication::applicationDirPath()+"/save"; //Windows保存
    w.show();
    return a.exec();
}

